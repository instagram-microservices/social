import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  const PORT = Number(process.env.PORT) || 3000;

  await app.listen(PORT);

  console.log(`Server listening on port ${PORT}`);
}
bootstrap();
