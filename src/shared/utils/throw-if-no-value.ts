import { Type } from '@nestjs/common';

export const throwIfNoValue = (error: Type<Error>) => {
  return <T>(item: T) => {
    if (!item) throw new error();

    return item;
  };
};
