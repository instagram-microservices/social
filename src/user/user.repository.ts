import { Injectable } from '@nestjs/common';
import { Neo4jService } from 'nest-neo4j/dist';

import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { head } from 'pratica';
import { FollowStats } from './entities/follow-stats';

@Injectable()
export class UserRepository {
  constructor(private neo4j: Neo4jService) {}

  public async create(createUserDto: CreateUserDto): Promise<User | null> {
    const { id, username } = createUserDto;

    const res = await this.neo4j.write(
      `CREATE (u:User {
        id: $id,
        username: $username
      })
      RETURN u`,
      { id, username },
    );

    return (
      head(res.records)
        .map((r) => r.get('u').properties as User)
        .value() ?? null
    );
  }

  public async follow(
    followerId: string,
    followedId: string,
  ): Promise<User | null> {
    const res = await this.neo4j.write(
      `MATCH (u:User {id: $followerId}), (target:User {id: $followedId})
      MERGE (u)-[r:FOLLOWS]->(target)
      ON CREATE SET r.createdAt = datetime()
      RETURN u, r, target`,
      { followerId, followedId },
    );

    return (
      head(res.records)
        .map((r) => r.get('target').properties as User)
        .value() ?? null
    );
  }

  public async unfollow(followerId: string, followedId: string): Promise<void> {
    await this.neo4j.write(
      `MATCH (u:User {id: $followerId})-[r:FOLLOWS]->(target:User {id: $followedId})
      DELETE r
      RETURN r`,
      { followerId, followedId },
    );
  }

  public async findFollowers(userId: string): Promise<User[]> {
    const res = await this.neo4j.read(
      `MATCH (f:User)-[:FOLLOWS]->(:User {id: $userId})
      RETURN f`,
      { userId },
    );

    return res.records.map((r) => r.get('f').properties);
  }

  public async findFollowing(userId: string): Promise<User[]> {
    const res = await this.neo4j.read(
      `MATCH (:User {id: $userId})-[:FOLLOWS]->(u:User)
      RETURN u`,
      { userId },
    );

    return res.records.map((r) => r.get('u').properties);
  }

  public async getFollowStats(userId: string): Promise<FollowStats | null> {
    const res = await this.neo4j.read(
      `MATCH (u:User {id: $userId})
      RETURN 
        size([(u)<-[r:FOLLOWS]-(follower:User) | follower]) as followers, 
        size([(u)-[r:FOLLOWS]->(following:User) | following]) as following`,
      { userId },
    );

    return (
      head(res.records)
        .map((r) => ({
          followers: r.get('followers').low,
          following: r.get('following').low,
        }))
        .value() ?? null
    );
  }

  public async delete(userId: string): Promise<void> {
    await this.neo4j.write(
      `MATCH (u:User {id: $userId})
      DETACH DELETE u
      RETURN u`,
      { userId },
    );
  }
}
