export class FollowStats {
  followers: number;

  following: number;
}
