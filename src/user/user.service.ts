import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(private userRepo: UserRepository) {}

  public create(createUserDto: CreateUserDto): Promise<any> {
    return this.userRepo.create(createUserDto);
  }

  public follow(followerId: string, followedId: string): Promise<any> {
    return this.userRepo.follow(followerId, followedId).then((user) => {
      if (!user) throw new NotFoundException('User not found');
      return user;
    });
  }

  public unfollow(followerId: string, followedId: string): Promise<any> {
    return this.userRepo.unfollow(followerId, followedId);
  }

  public getFollowers(followedId: string): Promise<User[]> {
    return this.userRepo.findFollowers(followedId);
  }

  public getFollowStats(userId: string) {
    return this.userRepo.getFollowStats(userId);
  }

  remove(userId: string) {
    return this.userRepo.delete(userId);
  }
}
