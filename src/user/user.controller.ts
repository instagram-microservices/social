import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Patch,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { CurrentUser } from './decorators/current-user.decorator';
import { User } from './entities/user.entity';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  public create(@Body() createUserDto: CreateUserDto) {
    return this.userService.create(createUserDto);
  }

  @Patch('followed/:id')
  public follow(@Param('id') followedId: string, @CurrentUser() user: User) {
    return this.userService.follow(user.id, followedId);
  }

  @Delete('followed/:id')
  public unfollow(@Param('id') followedId: string, @CurrentUser() user: User) {
    return this.userService.unfollow(user.id, followedId);
  }

  @Get('followers')
  public getFollowers(@CurrentUser() user: User) {
    return this.userService.getFollowers(user.id);
  }

  @Get('follow-stats')
  public getFollowStats(@CurrentUser() user: User) {
    return this.userService.getFollowStats(user.id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.userService.remove(id);
  }
}
