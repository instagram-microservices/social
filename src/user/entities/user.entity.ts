import { NodeEntity } from 'src/neo4j/decorators/node-entity';
import { NodeProp } from 'src/neo4j/decorators/node-prop';

@NodeEntity()
export class User {
  @NodeProp({ unique: true })
  id: string;

  @NodeProp({ unique: true })
  username: string;
}
