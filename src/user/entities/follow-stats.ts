export interface FollowStats {
  followers: number;
  following: number;
}
