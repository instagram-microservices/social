import { Module, OnModuleInit } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { UserRepository } from './user.repository';
import { Neo4jService } from 'nest-neo4j/dist';
import { Neo4jsModule } from 'src/neo4j/neo4js.module';
import { SchemaGenerator } from 'src/neo4j/decorators/schema-generator';
import { SchemaBuilder } from 'src/neo4j/decorators/schema-builder';
import { User } from './entities/user.entity';

@Module({
  controllers: [UserController],
  providers: [UserService, UserRepository],
  imports: [Neo4jsModule],
})
export class UserModule implements OnModuleInit {
  constructor(private readonly neo4jService: Neo4jService) {}

  async onModuleInit() {
    await new SchemaGenerator(new SchemaBuilder(), this.neo4jService).for(User);
  }
}
