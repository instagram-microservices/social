import { Module } from '@nestjs/common';
import { Neo4jRepo } from './neo4js.repo';

@Module({
  providers: [Neo4jRepo],
  exports: [Neo4jRepo],
})
export class Neo4jsModule {}
