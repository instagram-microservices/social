export const mergeOptions = <Options>(
  defaultOptions: Required<Options>,
  actualOptions?: Partial<Options>,
): Options => {
  if (!actualOptions) {
    return defaultOptions;
  }

  return Object.entries<Options[keyof Options]>(defaultOptions).reduce(
    (acc, [key, defaultValue]) => {
      const actualValue = actualOptions[key];

      acc[key] = actualValue === undefined ? defaultValue : actualValue;

      return acc;
    },

    {} as Options,
  );
};
