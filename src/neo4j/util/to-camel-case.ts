export const toCamelCase = (words: string[]): string => {
  return words
    .map((word, i) => {
      if (i === 0) {
        return word.toLowerCase();
      }

      return word[0].toUpperCase() + [...word].slice(1).join('').toLowerCase();
    })
    .join('');
};
