import { Neo4jOperator, Neo4jOperatorEnum } from './neo4j-operator';

export const Like = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.LIKE, [key, value]);
};

export const StartsWith = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.STARTS_WITH, [key, value]);
};

export const EndsWith = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.ENDS_WITH, [key, value]);
};

export const Contains = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.CONTAINS, [key, value]);
};

export const GreaterThan = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.GREATER_THAN, [key, value]);
};

export const GreaterThanOrEqual = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.GREATER_THAN_OR_EQUAL, [
    key,
    value,
  ]);
};

export const LessThan = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.LESS_THAN, [key, value]);
};

export const LessThanOrEqual = (key: string, value: any): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.LESS_THAN_OR_EQUAL, [key, value]);
};

export const Between = (
  key: string,
  floor: any,
  ceiling: any,
): Neo4jOperator => {
  return new Neo4jOperator(Neo4jOperatorEnum.BETWEEN, [key, floor, ceiling]);
};

export const Not = (operator: Neo4jOperator): Neo4jOperator => {
  return new Neo4jOperator(operator.type, operator.params, true);
};
