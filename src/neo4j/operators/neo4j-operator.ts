import { toCamelCase } from '../util/to-camel-case';

export class Neo4jOperator {
  constructor(
    public type: Neo4jOperatorEnum,
    public params: any[],
    public negated = false,
  ) {}
}

export const getMethodName = (operator: Neo4jOperator): string => {
  const not = operator.negated ? `NOT_` : '';

  const name = `${not}${operator.type}`;

  return toCamelCase(name.split('_'));
};

export enum Neo4jOperatorEnum {
  LIKE = 'LIKE',
  CONTAINS = 'CONTAINS',
  STARTS_WITH = 'STARTS_WITH',
  ENDS_WITH = 'ENDS_WITH',
  GREATER_THAN = 'GREATER_THAN',
  GREATER_THAN_OR_EQUAL = 'GREATER_THAN_OR_EQUAL',
  LESS_THAN = 'LESS_THAN',
  LESS_THAN_OR_EQUAL = 'LESS_THAN_OR_EQUAL',
  BETWEEN = 'BETWEEN',
}
