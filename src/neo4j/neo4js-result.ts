import { Integer, Node, QueryResult, Relationship } from 'neo4j-driver';

export class Neo4jsResult<T extends Record<string, any>> {
  constructor(
    private promise: Promise<QueryResult<any>>,
    private keys: string[],
  ) {}

  public getProps(alias: string): Promise<T> {
    return this.promise.then((res) => {
      return res.records[0].get(alias).properties;
    });
  }

  public getManyProps(alias: string): Promise<T[]> {
    return this.promise.then((res) => {
      return res.records.map((r) => r.get(alias).properties);
    });
  }

  public getNode(alias: string): Promise<Node<Integer, T>> {
    return this.promise.then((res) => {
      return res.records[0].get(alias);
    });
  }

  public getManyNodes(alias: string): Promise<Node<Integer, T>[]> {
    return this.promise.then((res) => {
      return res.records.map((r) => r.get(alias));
    });
  }

  public mapToNodes(): Promise<Record<string, Node | Relationship>[]> {
    return this.promise.then((res) => {
      return res.records.map((r) => r.toObject());
    });
  }

  public mapToProps(): Promise<Record<string, any>[]> {
    const keys = this.keys;

    return this.promise.then((res) => {
      return res.records.map((r) =>
        keys.reduce(function (acc, key) {
          acc[key] = r.get(key).properties;
          return acc;
        }, {} as Record<string, any>),
      );
    });
  }
}
