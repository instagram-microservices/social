import { mergeOptions } from '../util/merge-options';
import { metadata } from './metadata';

export interface PropMetadata {
  name: string;
  options: PropOptions;
}

export interface PropOptions {
  required: boolean;
  unique: boolean;
}

const defaultPropOptions: PropOptions = {
  required: true,
  unique: false,
};

export const NodeProp = (options?: Partial<PropOptions>): PropertyDecorator => {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return (target: Object, propertyKey: string): void => {
    const actualOptions = mergeOptions(defaultPropOptions, options);

    metadata.propModels.push({ options: actualOptions, name: propertyKey });
  };
};
