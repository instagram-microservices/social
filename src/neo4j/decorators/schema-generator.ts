import { Type } from '@nestjs/common';
import { Neo4jService } from 'nest-neo4j/dist';
import { metadata } from './metadata';
import { SchemaBuilder } from './schema-builder';

export class SchemaGenerator {
  constructor(
    private readonly schemaBuilder: SchemaBuilder,
    private readonly neo4jService: Neo4jService,
  ) {}

  public async for<T>(model: Type<T>): Promise<void> {
    const nodeMetaData = metadata.nodeModels.find(
      (node) => node.label === model.name,
    );

    if (!nodeMetaData) {
      throw new Error(`Model ${model.name} not found`);
    }

    const promises = this.schemaBuilder
      .createNodeSchema(nodeMetaData)
      .map(async (constraint) => {
        console.log(constraint);

        await this.neo4jService.write(constraint);
      });

    await Promise.all(promises);
  }
}
