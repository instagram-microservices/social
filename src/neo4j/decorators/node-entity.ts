import { metadata } from './metadata';
import { PropMetadata } from './node-prop';

export const NodeEntity = (label?: string): ClassDecorator => {
  // eslint-disable-next-line @typescript-eslint/ban-types
  return <TFunction extends Function>(target: TFunction): TFunction | void => {
    const actualLabel = label ?? target.name;

    const currentProps = metadata.propModels;

    metadata.nodeModels.push({
      label: actualLabel,
      props: currentProps,
    });

    metadata.propModels = [];
  };
};

export interface NodeModelMetadata {
  label: string;
  props: PropMetadata[];
}
