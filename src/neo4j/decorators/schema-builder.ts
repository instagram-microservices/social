import { NodeModelMetadata } from './node-entity';
import { PropMetadata } from './node-prop';

export enum ConstraintType {
  UNIQUE = 'UNIQUE',
  NOT_NULL = 'NOT_NULL',
}

export class SchemaBuilder {
  public createNodeSchema(node: NodeModelMetadata): string[] {
    return node.props.flatMap((prop) => this.createPropSchema(node, prop));
  }

  private createPropSchema(
    node: NodeModelMetadata,
    prop: PropMetadata,
  ): string[] {
    const { label } = node,
      alias = label[0].toLowerCase();

    const constraints: string[] = [];

    if (prop.options.required) {
      constraints.push(
        this.createConstraint(alias, label, prop.name, ConstraintType.NOT_NULL),
      );
    }

    if (prop.options.unique) {
      constraints.push(
        this.createConstraint(alias, label, prop.name, ConstraintType.UNIQUE),
      );
    }

    return constraints;
  }

  private createConstraint(
    alias: string,
    label: string,
    propName: string,
    type: ConstraintType,
  ): string {
    const constraintType = this.getConstraintType(type);

    return `CREATE CONSTRAINT IF NOT EXISTS FOR (${alias}:${label}) REQUIRE ${alias}.${propName} IS ${constraintType}`;
  }

  private getConstraintType(type: ConstraintType): string {
    const map: Record<ConstraintType, string> = {
      NOT_NULL: 'NOT NULL',
      UNIQUE: 'UNIQUE',
    };

    return map[type];
  }
}
