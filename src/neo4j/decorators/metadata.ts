import { NodeModelMetadata as NodeMetadata } from './node-entity';
import { PropMetadata } from './node-prop';

export interface Neo4jsMetadata {
  nodeModels: NodeMetadata[];
  propModels: PropMetadata[];
}

export const metadata: Neo4jsMetadata = {
  nodeModels: [],
  propModels: [],
};
