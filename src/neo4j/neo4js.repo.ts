import { Injectable, Type } from '@nestjs/common';
import { Neo4jService } from 'nest-neo4j/dist';
import { Neo4jsResult } from './neo4js-result';
import { NeoQueryBuilder } from './query-builder/neo-query-builder';

@Injectable()
export class Neo4jRepo {
  constructor(private neo4jService: Neo4jService) {}

  public create<T extends Record<string, any> = Record<string, any>>(
    type: Type<T>,
    props: Partial<T>,
    alias: string,
  ): Neo4jsResult<T> {
    return this.createQueryBuilder()
      .create(alias, type.name, props)
      .return(alias)
      .executeWrite();
  }

  public createQueryBuilder<
    T extends Record<string, any> = Record<string, any>,
  >(): NeoQueryBuilder<T> {
    return new NeoQueryBuilder(this.neo4jService);
  }
}
