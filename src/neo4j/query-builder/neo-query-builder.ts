import Builder, {
  Direction,
  WhereStatement,
  Order,
  StatementPrefix,
} from '@neode/querybuilder';
import Raw from '@neode/querybuilder/dist/raw';
import { Type } from '@nestjs/common';
import { Neo4jService } from 'nest-neo4j/dist';
import { Neo4jsResult } from '../neo4js-result';
import { getMethodName, Neo4jOperator } from '../operators/neo4j-operator';
import { ExtendedStatement } from './extended-statement';
import { Node, Integer } from 'neo4j-driver';
import { PatternBuilder } from './pattern-builder';
import { IfAny } from '../util/util-types';

interface BuiltCypher {
  cypher: string;
  params: object;
}

interface Params {
  [key: string]: any;
}

export type NodeType<
  Alias extends string,
  Model extends Record<string, any>,
> = Record<Alias, Node<Integer, Model>>;

export class NeoQueryBuilder<
  T extends Record<string, any> = Record<string, any>,
> {
  private builder = new Builder<T>();
  private queryRunner!: Neo4jService;
  private returnVars: string[] = [];

  constructor(queryRunner?: Neo4jService) {
    if (queryRunner) {
      this.queryRunner;
    }
  }

  public match1<Alias extends string, Model extends Record<string, any>>(
    alias: Alias,
    label: Type<Model>,
    properties: Partial<Model>,
  ): NeoQueryBuilder<
    IfAny<
      T,
      { [K in Alias]: Node<Integer, Model> },
      {
        [K in keyof (T & NodeType<Alias, Model>)]: (T &
          NodeType<Alias, Model>)[K];
      }
    >
  > {
    this.match(alias, label.name, properties);

    return this;
  }

  public match2<Alias extends string, Model extends Record<string, any>>(
    aliasAndLabel: { [K in Alias]: Type<Model> },
    properties: Partial<Model>,
  ): NeoQueryBuilder<
    IfAny<
      T,
      { [K in Alias]: Node<Integer, Model> },
      {
        [K in keyof (T & NodeType<Alias, Model>)]: (T &
          NodeType<Alias, Model>)[K];
      }
    >
  > {
    this.match(
      Object.keys(aliasAndLabel)[0],
      (Object.values(aliasAndLabel)[0] as Type<Model>).name,
      properties,
    );

    return this;
  }

  public match(
    alias: string,
    labels?: Array<string> | string,
    properties?: object,
  ): NeoQueryBuilder<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const builder: any = this.builder;

    builder.statements.push(new ExtendedStatement(StatementPrefix.MATCH));

    builder
      .currentStatement()
      .match(alias, labels, builder.aliasProperties(alias, properties));

    return this;
  }

  addMatch(
    alias: string,
    labels?: Array<string> | string,
    properties?: object,
  ): NeoQueryBuilder<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const builder: any = this.builder;

    builder
      .currentStatement()
      .addMatch(alias, labels, builder.aliasProperties(alias, properties));

    return this;
  }

  setParam(key: string, value: any): NeoQueryBuilder<T> {
    this.builder.setParam(key, value);
    return this;
  }

  call(fn: string, ...parameters: any[]): NeoQueryBuilder<T> {
    this.builder.match(fn, ...parameters);
    return this;
  }

  yield(...items: string[]): NeoQueryBuilder<T> {
    this.builder.yield(...items);
    return this;
  }

  optionalMatch(
    alias: string,
    labels?: Array<string> | string,
    properties?: object,
  ): NeoQueryBuilder<T> {
    this.builder.optionalMatch(alias, labels, properties);
    return this;
  }

  create(
    alias: string,
    labels?: Array<string> | string,
    properties?: object,
  ): NeoQueryBuilder<T> {
    this.builder.create(alias, labels, properties);
    return this;
  }

  merge(
    alias: string,
    labels?: Array<string> | string,
    properties?: object,
  ): NeoQueryBuilder<T> {
    this.builder.merge(alias, labels, properties);
    return this;
  }

  merge1(pattern: PatternBuilder): NeoQueryBuilder<T> {
    const [alias, labels, properties] = pattern.firstNode;

    this.builder.merge(alias, labels, properties);

    pattern.patternFactories.forEach((pattern) => pattern(this.builder));

    return this;
  }

  relationship(
    type: string | string[],
    direction?: Direction,
    alias?: string | null,
    properties?: object | undefined,
    degrees?: number | string,
  ): NeoQueryBuilder<T> {
    this.builder.relationship(type, direction, alias, properties, degrees);
    return this;
  }

  to(
    alias?: string | undefined,
    labels?: Array<string> | string,
    properties?: object | undefined,
  ): NeoQueryBuilder<T> {
    this.builder.to(alias, labels, properties);
    return this;
  }

  onCreateSet(key: string, value: any): NeoQueryBuilder<T> {
    this.builder.onCreateSet(key, value);
    return this;
  }

  onMatchSet(key: string, value: any): NeoQueryBuilder<T> {
    this.builder.onMatchSet(key, value);
    return this;
  }

  set(key: string, value: any): NeoQueryBuilder<T> {
    this.builder.set(key, value);
    return this;
  }

  remove(...values: string[]) {
    this.builder.remove(...values);
    return this;
  }

  setAppend(key: string, value: object): NeoQueryBuilder<T> {
    this.builder.setAppend(key, value);
    return this;
  }

  where(
    key: string | WhereStatement | object,
    value?: any | undefined,
  ): NeoQueryBuilder<T> {
    this.builder.where(key, value);
    return this;
  }

  whereNot(key: string | object, value?: any | undefined): NeoQueryBuilder<T> {
    this.builder.whereNot(key, value);
    return this;
  }

  whereId(alias: string, id: number): NeoQueryBuilder<T> {
    this.builder.whereId(alias, id);
    return this;
  }

  whereNotId(alias: string, id: number): NeoQueryBuilder<T> {
    this.builder.whereNotId(alias, id);
    return this;
  }

  whereRaw(predicate: string): NeoQueryBuilder<T> {
    this.builder.whereRaw(predicate);
    return this;
  }

  whereOp(operator: Neo4jOperator): NeoQueryBuilder<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const builder: any = this.builder;

    builder[getMethodName(operator)](...operator.params);

    return this;
  }

  delete(...items: string[]): NeoQueryBuilder<T> {
    this.builder.delete(...items);

    return this;
  }

  detachDelete(...items: string[]): NeoQueryBuilder<T> {
    this.builder.detachDelete(...items);
    return this;
  }

  orderBy(key: string, order: Order = Order.ASC): NeoQueryBuilder<T> {
    this.builder.orderBy(key, order);

    return this;
  }

  or(key: string | object, value?: any | undefined): NeoQueryBuilder<T> {
    this.builder.or(key, value);

    return this;
  }

  skip(skip: number): NeoQueryBuilder<T> {
    this.builder.skip(skip);

    return this;
  }

  limit(limit: number): NeoQueryBuilder<T> {
    this.builder.limit(limit);

    return this;
  }

  return(...values: Array<string>): NeoQueryBuilder<T> {
    this.builder.return(...values);
    this.returnVars = values;
    return this;
  }

  public return1<K extends T>(
    ...vars: (keyof K)[]
  ): NeoQueryBuilder<{ [P in keyof T]: T[P] }> {
    return this;
  }

  with(...items: string[]): NeoQueryBuilder<T> {
    this.builder.with(...items);
    return this;
  }

  foreach(
    identifier: string,
    collection: any[],
    query: Builder<T>,
  ): NeoQueryBuilder<T> {
    this.builder.foreach(identifier, collection, query);

    return this;
  }

  raw(value: any): Raw {
    return this.builder.raw(value);
  }

  public getQuery(): string {
    return this.builder.toString();
  }

  public getParams(): Params {
    return this.builder.getParams();
  }

  public getQueryAndParams(): BuiltCypher {
    return {
      cypher: this.getQuery(),
      params: this.getParams(),
    };
  }

  public executeRead<
    T extends Record<string, any> = Record<string, any>,
  >(): Neo4jsResult<T> {
    const { cypher, params } = this.getQueryAndParams();

    return new Neo4jsResult<T>(
      this.queryRunner.read(cypher, params),
      this.returnVars,
    );
  }

  public executeWrite<
    T extends Record<string, any> = Record<string, any>,
  >(): Neo4jsResult<T> {
    const { cypher, params } = this.getQueryAndParams();

    return new Neo4jsResult<T>(
      this.queryRunner.write(cypher, params),
      this.returnVars,
    );
  }
}
