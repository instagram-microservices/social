import Statement from '@neode/querybuilder/dist/statements/statement';
import MatchStatement from '@neode/querybuilder/dist/statements/match-statement';
import ExtendedMatchStatement from './extended-match-statement';
import { StatementPrefix } from '@neode/querybuilder';

export class ExtendedStatement<T> extends Statement<T> {
  constructor(prefix: StatementPrefix) {
    super(prefix);
  }

  match(alias: string | undefined, labels: any, properties: any): Statement<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const builder = this;

    (builder as any).pattern.push(
      new ExtendedMatchStatement(new MatchStatement(alias, labels, properties)),
    );

    return this;
  }

  addMatch(
    alias: string | undefined,
    labels: any,
    properties: any,
  ): Statement<T> {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const builder = this;

    const latestStatement = (builder as any).pattern.at(-1);

    if (
      !latestStatement ||
      !(latestStatement instanceof ExtendedMatchStatement)
    ) {
      return this;
    }

    latestStatement.addMatch(new MatchStatement(alias, labels, properties));

    return this;
  }
}
