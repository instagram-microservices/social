import MatchStatement from '@neode/querybuilder/dist/statements/match-statement';

export default class ExtendedMatchStatement {
  private matchStatements: MatchStatement<any>[];

  constructor(statement: MatchStatement<any>) {
    this.matchStatements = [statement];
  }

  public toString(): string {
    return this.matchStatements.map((st) => st.toString()).join(', ');
  }

  public addMatch(statement: MatchStatement<any>): this {
    this.matchStatements.push(statement);

    return this;
  }
}
