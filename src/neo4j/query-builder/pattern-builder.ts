import QueryBuilder, { Direction } from '@neode/querybuilder';

export class PatternBuilder {
  public readonly firstNode: [
    string,
    string | string[] | undefined,
    object | undefined,
  ];

  public readonly patternFactories: Array<(qb: QueryBuilder<any>) => void> = [];

  constructor(
    alias: string,
    labels?: Array<string> | string,
    properties?: object | undefined,
  ) {
    this.firstNode = [alias, labels, properties];
  }

  public out(
    type: string | string[],
    alias?: string | null,
    properties?: object | undefined,
    degrees?: number | string,
  ): PatternBuilder {
    this.patternFactories.push((qb) => {
      qb.relationship(type, Direction.OUTGOING, alias, properties, degrees);
    });

    return this;
  }

  public in(
    type: string | string[],
    alias?: string | null,
    properties?: object | undefined,
    degrees?: number | string,
  ): PatternBuilder {
    this.patternFactories.push((qb) => {
      qb.relationship(type, Direction.INCOMING, alias, properties, degrees);
    });

    return this;
  }

  public outAndIn(
    type: string | string[],
    alias?: string | null,
    properties?: object | undefined,
    degrees?: number | string,
  ): PatternBuilder {
    this.patternFactories.push((qb) => {
      qb.relationship(type, Direction.INCOMING, alias, properties, degrees);
    });

    return this;
  }

  to(
    alias?: string | undefined,
    labels?: Array<string> | string,
    properties?: object | undefined,
  ): PatternBuilder {
    this.patternFactories.push((qb) => {
      qb.to(alias, labels, properties);
    });

    return this;
  }
}

export const node = (
  alias: string,
  labels?: Array<string> | string,
  properties?: object | undefined,
) => new PatternBuilder(alias, labels, properties);
